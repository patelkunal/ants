package pack;

import static org.junit.Assert.*;

import org.junit.Test;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class GreeterTest {

	@Test
	public void testGreet(){
		String output = new Greeter().greet();
		// System.out.println("Actual output = " + output);
		assertEquals("hello", output);
	}

	@Test
	public void testGreet_v2() throws IOException {
		String output = new Greeter().greet();
		System.out.println("Actual output = " + output);
		
		String expectedOutput = readFromResource();
		System.out.println("Expected output = " + expectedOutput);
		assertEquals(expectedOutput, output);	
	}

	public String readFromResource() throws IOException{
		String file = this.getClass().getResource("/greeting.message").getFile();
		System.out.println(file);
		InputStream inStream = new FileInputStream(file);
		return IOUtils.toString(inStream, "UTF-8");
		
		// return IOUtils.toString(new FileInputStream(this.getClass().getResource("/greeting.message").getFile()), "UTF-8");
	}

}